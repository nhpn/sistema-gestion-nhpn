import { Sequelize } from "sequelize";
import { DB_URL, NODE_ENV } from "./constants.js";


export const sequelize = new Sequelize(DB_URL, {
  dialect: "postgres",
  protocol: "postgres",
  logging: false,
  dialectOptions: NODE_ENV
});

export async function connectDB(){
  try {
    await sequelize.authenticate();
    console.log("Connection DB has been established successfully.");
  } catch (e) {
    console.log(`Unable to connect to the database ${e}`);
  }
}
