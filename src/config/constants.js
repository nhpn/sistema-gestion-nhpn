import "dotenv/config";

export const DB_URL = process.env.DB_URL || "";
export const NODE_ENV = process.env.NODE_ENV || "";