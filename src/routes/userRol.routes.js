import UserRolController from "../controllers/userRol.controller.js";
import { Router } from "express";

const userRolRouter = Router();
const userRolController = new UserRolController();

userRolRouter.post("/createRol", userRolController.createUserRol);

export default userRolRouter;