import UserController from "../controllers/user.controller.js";
import { Router } from "express";

const router = Router();
const userController = new UserController();

router.get("/list", userController.getUsers);
router.post("/create", userController.createUser);

export default router;