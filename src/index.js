import app from "./app.js";
import { connectDB } from "./config/database.js";
import "./models/User.js";
import "./models/UserRol.js";

connectDB();

const PORT = 3000;


app.listen(PORT, () => {
    console.log(`Servidor corriendo en http://localhost:${PORT}`);
});
