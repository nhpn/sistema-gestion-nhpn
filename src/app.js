import express from "express";
import router from "./routes/user.routes.js";
import userRolRouter from "./routes/userRol.routes.js"; "./routes/userRol.routes.js";

const app = express();

app.use(express.json());
app.use('/api/v1',router);
app.use('/api/v1',userRolRouter);

export default app;
