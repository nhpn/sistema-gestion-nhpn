import UserRolService from "../services/userRol.service.js";

const _userRolService = new UserRolService();

class UserRolController{

    async createUserRol (req,res) {
        try {
          const rol = await _userRolService.create(req.body);
    
          return res.status(201).json({
            message: "User rol added",
            data: rol,
          });
        } catch (e) {
          return res.status(400).json({
            message: req.body,
          });
        }
      };
}

export default UserRolController;