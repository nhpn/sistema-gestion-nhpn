import UserService from "../services/user.service.js";

const _userService = new UserService();

class UserController{

    async createUser(req, res) {
        try {
            const newUser = await _userService.createUser(req.body);
            console.log(newUser);
            return res.status(201).json({
                data: newUser,
            });
        }catch(e) {
            return res.status(400).json({
              message: e,
            });
        }
    }

    async getUsers (req, res) {
        try{
            const users = await _userService.findAll();
            return res.status(200).json({
                message: "Successfully fetched users",
                data: users
              });
        }catch(e) {
            return res.status(400).json({
              message: e,
            });

        }
    }

}

export default UserController;