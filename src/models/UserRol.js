import { sequelize } from "../config/database.js";
import { Sequelize } from "sequelize";

const UserRol = sequelize.define("user_rols",{
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
    name: {
        type: Sequelize.STRING,
        allowNull: false,
     },
    },
    {
        timestamps:false,
    }
);

UserRol.sync({ force: false, alter: true });

export default UserRol;
