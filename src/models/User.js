import { sequelize } from "../config/database.js";
import bcrypt from "bcrypt";
import { Sequelize } from "sequelize";
import UserRol from "./UserRol.js";


const User = sequelize.define(
  "users",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    lastname: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    rut: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
    },
    email: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    password:{
      type: Sequelize.STRING,
      allowNull:false
    }
  },
  {
    updatedAt: false,
    hooks: {
      beforeSave: async (user) => {
        const password = user.get().password;
        if (password) {
          const salt = await bcrypt.genSalt(10);
          const hashPassword = bcrypt.hashSync(password, salt);
          user.set("password", hashPassword);
        }
      },
    },
  }
);

UserRol.hasMany(User, {
  foreignKey: "rol",
  sourceKey: "id",
});

User.belongsTo(UserRol, {
  foreignKey: "rol",
  targetKey: "id",
  as: "user_rols",
});

User.sync({ force: false, alter: true });

export default User;
