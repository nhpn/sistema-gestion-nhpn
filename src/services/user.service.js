import User from "../models/User.js";

class UserService{

    async createUser(user) {
        await User.create(user);
    }

    async findAll() {
        const all = await User.findAll({
            attributes: {
            exclude: ["password"],
          }
        });
        return all;
    }

}

export default UserService;