import UserRol from "../models/UserRol.js";

class UserRolService{
    async create (rol){
        await UserRol.create(rol);
    }
}

export default UserRolService;